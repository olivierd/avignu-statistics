Dépôt hébergeant différents scripts dans le but d'obtenir des graphiques (afin d'illustrer le rapport d'activité de l'association [AviGNU](https://avignu.com/) sur l'évolution du nombre d'adhérents.

## Les prérequis

- LibreOffice
- Python 3.x
- R
- GNU Make (optionnel)

**Note :** Sous Debian, il faut vérifier que ces paquets soient installés.

- `r-base-core`
- `libunoloader-java`
- `libreoffice-java-common`
- `liblibreoffice-java`

Les informations sont récupérées depuis un fichier `.ods` (tableur LibreOffice).

La structure est la suivante :

```
+---+-----+--------+-------+------------+--------------------+------+
|   |  A  |   B    |   C   |     D      |         E          |  F   |
+---+-----+--------+-------+------------+--------------------+------+
| 1 | Nom | Prénom | Email | Cotisation | Liste de diffusion | Date |
+---+-----+--------+-------+------------+--------------------+------+
```

- Les champs **A** et **B** sont **obligatoires**
- Le champ **C** peut être vide (dans ce cas **E**, le sera aussi)
- Les champs **D** et **E** ont 2 valeurs possibles → ok ou vide
- Le champ **F** peut être vide
