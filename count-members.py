#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2024, Olivier Duchateau <duchateau.olivier@gmail.com>
#

# Count members from the last sheet.
#

import argparse
import mimetypes
import pathlib

from members import MembersFromSpreadSheet
from writetocalc import WriteToCalc

def is_spreadsheet(obj):
    res = False

    mime_type, encoding = mimetypes.guess_type(obj)
    if 'oasis.opendocument.spreadsheet' in mime_type:
        res = True

    return res

def normalize_path(path):
    '''Return an absolute pathlib.Path object'''

    if path.startswith('~'):
        return pathlib.Path(path).expanduser()
    else:
        return pathlib.Path(path).resolve()

def main(args):
    if args.output is None:
        dir_name = pathlib.Path('./members').resolve()
        output_filename = dir_name.joinpath('members-by-year.ods')
    else:
        output_filename = normalize_path(args.output)
    if output_filename.exists():
        new = False
    else:
        new = True

    input = normalize_path(args.file)
    if is_spreadsheet(input) and input.exists():
        m = MembersFromSpreadSheet(input.as_uri(), args.strict)

        # Only members from the last sheet
        members = m.get_members(-1)

        m.close()

        if isinstance(members, tuple):
            w = WriteToCalc(output_filename.as_uri(), new)
            #w.do_write_count(members)
            w.do_write_with_header(members, 'Année', 'Adhérents')
            w.close()
    else:
        print('error: not a valid file')
        sys.exit(-1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', required=True,
                        help='LibreOffice spreadsheet file')
    parser.add_argument('-o', '--output',
                        help='file to store result')
    parser.add_argument('-s', '--strict', action='store_true',
                        help='only members with current membership fees')
    main(parser.parse_args())
