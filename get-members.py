#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2024, Olivier Duchateau <duchateau.olivier@gmail.com>
#

# Get members list from the last n sheets.
#

import argparse
import mimetypes
import pathlib

from members import AviGNUSpreadSheet
from writetocalc import WriteToCalc

class MembersLastYear(AviGNUSpreadSheet):
    '''Class to get members from the last year.'''

    def __init__(self, uri, exclude_last):
        self.__last = exclude_last

        super().__init__(uri, True, exclude_last_sheet=self.__last)
        self.__n_sheets = self.document.Sheets.Count

    def get_sheets(self):
        '''Return only only one sheet'''

        if self.__n_sheets == 1:
            return self.sheets[0]
        elif self.__n_sheets >= 2:
            if self.exclude_last:
                return self.sheets[(self.__n_sheets - 2)]
            else:
                return self.sheets[(self.__n_sheets - 1)]

def is_spreadsheet(obj):
    res = False

    mime_type, encoding = mimetypes.guess_type(obj)
    if 'oasis.opendocument.spreadsheet' in mime_type:
        res = True

    return res

def normalize_path(path):
    '''Return an absolute pathlib.Path object'''

    if path.startswith('~'):
        return pathlib.Path(path).expanduser()
    else:
        return pathlib.Path(path).resolve()

def main(args):
    sep = ';;;'

    if args.output is None:
        dir_name = pathlib.Path.home()
        output_filename = dir_name.joinpath('adherents.ods')
    else:
        output_filename = normalize_path(args.output)
    if output_filename.exists():
        new = False
    else:
        new = True

    input = normalize_path(args.file)
    if is_spreadsheet(input) and input.exists():
        m = MembersLastYear(input.as_uri(), args.exclude_last)
        m.delimiter = sep

        members = m.get_members()
        m.close()
        if members is not None:
            #print(members)
            w = WriteToCalc(output_filename.as_uri(), new)
            w.do_write_names(members, sep, 'Nom', 'Prénom')
            w.close()
    else:
        print('error: not a valid file')
        sys.exit(-1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', required=True, dest='file',
                        help='LibreOffice spreadsheet file')
    parser.add_argument('-s', '--strict', action='store_true',
                        help='only members with current membership fees')
    parser.add_argument('-e', '--exclude-last', action='store_true',
                        dest='exclude_last',
                        help='exclude the last spreadsheet')
    parser.add_argument('-o', '--output',
                        help='file to store result')
    main(parser.parse_args())
