# -*- coding: utf-8 -*-

# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2024, Olivier Duchateau <duchateau.olivier@gmail.com>
#

# Base class to search members from LibreOffice spreadsheet.
#

import shutil
import subprocess
import sys
import time

try:
    import uno
    from com.sun.star.table.CellContentType import EMPTY
    from com.sun.star.table.CellContentType import TEXT
except ImportError as err:
    print('the python UNO bridge is required')
    sys.exit(-1)


class AviGNUSpreadSheet(object):

    def __init__(self, uri, strict, sep=';;', exclude_last_sheet=True):
        if sep is not None or sep != '':
            self.delimiter = sep
        else:
            self.delimiter = ';;'

        self.exclude_last = exclude_last_sheet

        self.proc = self.__launch_libreoffice()
        # Wait for LibreOffice to start
        time.sleep(5)

        if self.proc.returncode is None:
            self.strict = strict

            self.document = self.__open_file(uri)
            self.sheets = self.document.Sheets.ElementNames
        else:
            raise subprocess.CalledProcessError
            sys.exit(-1)

    def __launch_libreoffice(self, component='--calc'):
        '''Launch one instance of LibreOffice, using named pipe.'''

        params = '--accept=pipe,name=unopipe;urp;'

        # Full command line 
        cli = [shutil.which('soffice'), component, params, '--headless']
        try:
            process = subprocess.Popen(cli)
        except OSError as err:
            raise(err)

        return process

    def __open_file(self, resource):
        ctx = uno.getComponentContext()
        resolver = ctx.ServiceManager.createInstanceWithContext('com.sun.star.bridge.UnoUrlResolver',
                                                                ctx)
        conn = resolver.resolve('uno:pipe,name=unopipe;urp;StarOffice.ComponentContext')
        service = conn.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop',
                                                                conn)

        return service.loadComponentFromURL(resource, '_blank', 0, ())

    def __get_cell_content(self, cursor, cell_name):
        if cursor.getCellRangeByName(cell_name).CellContentType != EMPTY:
            return cursor.getCellRangeByName(cell_name).String
        else:
            return None

    def __is_cell_content(self, cursor, cell_name):
        if cursor.getCellRangeByName(cell_name).CellContentType != EMPTY:
            return True
        else:
            return False

    def __cursor(self, name):
        sheet = self.document.Sheets.getByName(name)

        cursor = sheet.createCursor()
        cursor.gotoEndOfUsedArea(True)

        return cursor

    def get_emails_by_sheet(self, name):
        cursor = self.__cursor(name)

        _emails = set()
        _addr = None
        store = False
        i = 2

        while i <= cursor.getRows().Count:
            _addr = self.__get_cell_content(cursor, 'C{0}'.format(i))
            store = self.__is_cell_content(cursor, 'E{0}'.format(i))

            if store and _addr is not None:
                _emails.add(_addr)

            _addr = None
            store = False
            i += 1

        return _emails

    def __members_by_sheet(self, name):
        _members = set()
        cursor = self.__cursor(name)

        first_name = None
        last_name = None
        i = 2

        while i <= cursor.getRows().Count:
            last_name = self.__get_cell_content(cursor, 'A{0}'.format(i))
            first_name = self.__get_cell_content(cursor, 'B{0}'.format(i))

            # Membership fee is ok?
            if cursor.getCellRangeByName('D{0}'.format(i)).CellContentType == EMPTY:
                # Clear
                if self.strict:
                    last_name = None
                    first_name = None

            if last_name is not None and first_name is not None:
                _members.add('{0}{1}{2}'.format(last_name,
                                                self.delimiter, first_name))

            first_name = None
            last_name = None
            i += 1

        return _members

    def __members(self, sheets):
        '''Return set object where duplicates have been removed'''
        _list_members = []
        _merged = set()

        for sheet in sheets:
            _list_members.append(self.__members_by_sheet(sheet))

        # Remove duplicate elements
        for i in _list_members:
            _merged.update(i)

        return _merged

    def get_sheets(self):
        pass

    def get_members(self):
        list_members = None
        sheets = self.get_sheets()

        if sheets is not None:
            if isinstance(sheets, tuple):
                list_members = list(self.__members(sheets))
            else:
                list_members = list(self.__members_by_sheet(sheets))

        if list_members is not None:
            list_members.sort()

        return list_members

    def close(self):
        self.document.close(True)

        self.proc.terminate()
