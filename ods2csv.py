#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Unlicense
#
# Simple script which converts .ods spreadsheet to .csv
# (Comma-separated values).
# Be aware, only 2 types of values are exported.
#

import argparse
import csv
import mimetypes
import pathlib
import shutil
import subprocess
import sys
import time

try:
    import uno
    from com.sun.star.table.CellContentType import TEXT
    from com.sun.star.table.CellContentType import VALUE
except ImportError:
    print('The Python UNO bridge is required')
    sys.exit(-1)


def is_spreadsheet(filename):
    '''Check right MIME type.'''
    res = False

    mime_type, encoding = mimetypes.guess_type(filename)
    if 'oasis.opendocument.spreadsheet' in mime_type:
        res = True
    return res

def launch_libreoffice_calc():
    '''Launch LibreOffice instance with named pipes.'''
    accept_param = '--accept=pipe,name=unopipe;urp;'

    # Full command line
    cmd = [shutil.which('soffice'), '--calc', accept_param, '--headless']

    try:
        proc = subprocess.Popen(cmd)
    except OSError as err:
        raise(err)
    return proc

def connect_to_libreoffice():
    ctx = uno.getComponentContext()
    resolver = ctx.ServiceManager.createInstanceWithContext('com.sun.star.bridge.UnoUrlResolver',
                                                            ctx)
    conn = resolver.resolve('uno:pipe,name=unopipe;urp;StarOffice.ComponentContext')
    service = conn.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop',
                                                            conn)
    return service

def load_file(lo_obj, filename):
    document = None

    if isinstance(filename, pathlib.Path):
        document = lo_obj.loadComponentFromURL(filename.as_uri(),
                                               '_blank', 0, ())
    return document

def get_cursor(document):
    sheet = document.CurrentController.getActiveSheet()

    # Create cursor object
    cursor = sheet.createCursor()
    cursor.gotoEndOfUsedArea(True)
    return cursor

def get_rows_content(document, exclude_row, csv_obj):
    data = []

    cursor = get_cursor(document)

    # Row starts to indice 1
    i = 1
    while i <= cursor.getRows().Count:
        if exclude_row is not None and i == (exclude_row + 1):
            i += 1
        else:
            for col in cursor.getColumns().getElementNames():
                cell = '{0}{1}'.format(col, i)
                if cursor.getCellRangeByName(cell).CellContentType == VALUE:
                    data.append(cursor.getCellRangeByName(cell).Value)
                elif cursor.getCellRangeByName(cell).CellContentType == TEXT:
                    data.append(cursor.getCellRangeByName(cell).getString())
            i += 1

        if data:
            csv_obj.writerow(data)
        data = []

def ods2cvs(input_filename, output_filename, exclude_row):
    # Launch LibreOffice instance
    process = launch_libreoffice_calc()
    time.sleep(5)

    # Child process is running?
    if process.returncode is None:
        service = connect_to_libreoffice()
        document = load_file(service, input_filename)
        if document is not None:
            o = open(output_filename, 'w', newline='')
            writer = csv.writer(o, dialect='unix')

            get_rows_content(document, exclude_row, writer)

            o.close()
            document.close(True)
        process.terminate()

def normalize_path(filename):
    if filename.startswith('~'):
        return pathlib.Path(filename).expanduser()
    else:
        return pathlib.Path(filename).resolve()

def main(args):
    resource = normalize_path(args.file)
    # Build .csv file
    if args.output is None:
        dirname = pathlib.Path().home()
        csv_filename = dirname.joinpath(resource.name).with_suffix('.csv')
    else:
        csv_filename = normalize_path(args.output)

    # We have an OpenDocument Spreadsheet
    if is_spreadsheet(resource):
        if resource.exists():
            ods2cvs(resource, csv_filename, args.exclude)
        else:
            raise FileNotFoundError
            sys.exit(0)
    else:
        print('Expected OASIS OpenDocument Spreadsheet')
        sys.exit(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file',
                        help='.ods file',
                        required=True)
    parser.add_argument('-o', '--output',
                        help='.csv file')
    parser.add_argument('-e', '--exclude', type=int,
                        help='exclude row')
    main(parser.parse_args())
