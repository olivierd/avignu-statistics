# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2024, Olivier Duchateau <duchateau.olivier@gmail.com>
#

# Base class to write content into LibreOffice spreadsheet.
#

import shutil
import subprocess
import sys
import time

try:
    import uno
    from com.sun.star.awt.FontWeight import BOLD
except ImportError as err:
    print('the python uno bridge is required')
    sys.exit(-1)


class WriteToCalc(object):

    def __init__(self, uri, new):
        self.__new = new
        self.__resource = uri

        self.proc = self.__launch_libreoffice()
        # Wait for LibreOffice to start
        time.sleep(5)

        if self.proc.returncode is None:
            self.document = self.__open_file()
        else:
            raise subprocess.CalledProcessError
            sys.exit(-1)

    def __launch_libreoffice(self):
        '''Launch one instance of LibreOffice, using named pipe'''

        params = '--accept=pipe,name=unopipe;urp;'

        # Full command line
        cmd = [shutil.which('soffice'), '--calc', params,
               '--headless']
        try:
            process = subprocess.Popen(cmd)
        except OSError as err:
            raise(err)

        return process

    def __open_file(self):
        ctx = uno.getComponentContext()
        resolver = ctx.ServiceManager.createInstanceWithContext('com.sun.star.bridge.UnoUrlResolver',
                                                                ctx)
        conn = resolver.resolve('uno:pipe,name=unopipe;urp;StarOffice.ComponentContext')
        service = conn.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop',
                                                                conn)

        if self.__new:
            return service.loadComponentFromURL('private:factory/scalc',
                                                '_blank', 0, ())
        else:
            return service.loadComponentFromURL(self.__resource,
                                                '_blank', 0, ())

    def __get_last_row(self, sheet):
        cursor = sheet.createCursor()
        cursor.gotoEndOfUsedArea(True)

        # Find the last row
        range_addr = cursor.getCellRangeByName(cursor.AbsoluteName)

        return range_addr.RangeAddress.EndRow

    def __get_sheet(self, pos):
        # Usually we are working with one sheet
        return self.document.getSheets().getByIndex(pos)

    def __write_header(self, sheet, *labels):
        i = 0
        while i < len(labels):
            cell = sheet.getCellByPosition(i, 0)

            cursor = cell.createTextCursor()
            cursor.setPropertyValue('CharWeight', BOLD)
            cursor.setString(labels[i])

            i += 1

    def do_write_names(self, iterable, sep, *labels):
        sheet = self.document.CurrentController.ActiveSheet

        self.__write_header(sheet, *labels)

        for i in iterable:
            tokens = i.split(sep)

            last_row = (self.__get_last_row(sheet) + 1)

            sheet.getCellByPosition(0, last_row).setString(tokens[0])
            sheet.getCellByPosition(1, last_row).setString(tokens[1])

    #def do_write_count(self, iterable):
    #    '''Store integer combined with string'''
    #    sheet = self.__get_sheet(0)

    #    last_row = self.__get_last_row(sheet)

    #    if self.__new:
    #        pos_y = last_row
    #    else:
    #        pos_y = last_row + 1

    #    if isinstance(iterable, tuple):
            # First element is a string
    #        sheet.getCellByPosition(0,
    #                                pos_y).setString(iterable[0])

            # 2nd element is list or integer
    #        if isinstance(iterable[1], list):
    #            sheet.getCellByPosition(1,
    #                                    pos_y).setValue(len(iterable[1]))
    #        elif isinstance(iterable[1], int):
    #            sheet.getCellByPosition(1,
    #                                    pos_y).setValue(iterable[1])

    def do_write_with_header(self, iterable, *labels):
        sheet = self.__get_sheet(0)

        if self.__new:
            self.__write_header(sheet, *labels)
        self.do_write_count(iterable)

    def close(self):
        if self.__new:
            self.document.storeAsURL(self.__resource, ())
            self.document.storeToURL(self.__resource, ())
        else:
            self.document.store()

        self.document.close(True)

        self.proc.terminate()
