#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2025, Olivier Duchateau <duchateau.olivier@gmail.com>
#
# Emails to remove in our mailing list. Normally no results are expected.
#

import argparse
import os
import pathlib

from members import AviGNUSpreadSheet

class EmailsMembers(AviGNUSpreadSheet):
    def __init__(self, uri, start):
        super().__init__(uri, False, exclude_last_sheet=False)

        self.start = start
        self.n_sheets = self.document.Sheets.Count

    def get_sheets(self):
        '''Return a tuple (or a string, if one element)'''
        if self.n_sheets == 1:
            return self.sheets[0]
        elif self.n_sheets >= 2:
            return self.sheets[self.start:]

    def get_emails(self):
        list_emails = []

        for name in self.get_sheets():
            list_emails.append(self.get_emails_by_sheet(name))

        return list_emails

def normalize_path(path):
    '''Return an absolute pathlib.Path object'''
    if path.startswith('~'):
        return pathlib.Path(path).expanduser()
    else:
        return pathlib.Path(path).resolve()

def main(args):
    lo_file = normalize_path(args.file)
    plain_text = normalize_path(args.plain_text)

    if lo_file.exists() and plain_text.exists():
        m = EmailsMembers(lo_file.as_uri(), args.start_from)
        list_ = m.get_emails()
        m.close()

        if len(list_) == 2:
            emails_to_merge = set()

            emails_to_merge = list_[0].union(list_[1])

            with open(plain_text, 'r') as f:
                content = f.read().splitlines()

            emails_framagroupes = set(content)

            result = emails_framagroupes.difference(emails_to_merge)
            if result != set():
                print(result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', required=True,
                        help='LibreOffice spreadsheet file')
    parser.add_argument('-t', '--plain-text', required=True,
                        dest='plain_text',
                        help='Export file from Framagroupes')
    parser.add_argument('-s', '--start', type=int, default=0,
                        dest='start_from',
                        help='the first spreadsheet to start')
    main(parser.parse_args())
