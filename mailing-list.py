#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2024-2025, Olivier Duchateau <duchateau.olivier@gmail.com>
#

# Compare emails between several sheets, and store difference.
#

import argparse
import mimetypes
import os
import pathlib

from members import AviGNUSpreadSheet

class CleanMailingList(AviGNUSpreadSheet):

    def __init__(self, uri, start, exclude_last):
        super().__init__(uri, False, exclude_last_sheet=exclude_last)

        self.start = start
        self.exclude_last = exclude_last
        self.n_sheets = self.document.Sheets.Count

    def get_sheets(self):
        '''Return a tuple (or a string if one elemement).'''
        if self.n_sheets == 1:
            return self.sheets[0]
        elif self.n_sheets >= 2:
            if self.exclude_last:
                return self.sheets[self.start:(self.n_sheets - 1) - self.n_sheets]
            else:
                return self.sheets[self.start:]

    def get_emails(self):
        _list_emails = []

        for sheet_name in self.get_sheets():
            _list_emails.append(self.get_emails_by_sheet(sheet_name))

        return _list_emails

def is_spreadsheet(obj):
    res = False

    mime_type, encoding = mimetypes.guess_type(obj)
    if 'oasis.opendocument.spreadsheet' in mime_type:
        res = True

    return res

def normalize_path(path):
    '''Return an absolute pathlib.Path object'''

    if path.startswith('~'):
        return pathlib.Path(path).expanduser()
    else:
        return pathlib.Path(path).resolve()

def main(args):
    dirname = pathlib.Path.home()
    filename = dirname.joinpath('emails-a-supprimer.txt')

    input_file = normalize_path(args.file)
    if is_spreadsheet(input_file) and input_file.exists():
        c = CleanMailingList(input_file.as_uri(), args.start_from,
                             args.exclude_last)
        list_emails = c.get_emails()
        c.close()

        if len(list_emails) == 2:
            emails_to_remove = set()

            emails_to_remove = list_emails[0].difference(list_emails[1])

            with open(filename, 'w') as f:
                for i in emails_to_remove:
                    f.write('{0}{1}'.format(i, os.linesep))
            f.close()
        else:
            print('error')
    else:
        print('error: not a valid file')
        sys.exit(-1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', required=True,
                        help='LibreOffice spreadsheet file')
    parser.add_argument('-s', '--start', type=int, default=0,
                        dest='start_from',
                        help='the first spreadsheet to start')
    parser.add_argument('-e', '--exclude-last', dest='exclude_last',
                        action='store_true',
                        help='does not exclude the last spreadsheet')
    main(parser.parse_args())
